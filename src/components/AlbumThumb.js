// Import React stuff
import React from 'react';
import { Link } from 'react-router';

const AlbumThumb = props => (
  <Link to={props.name} className="thumb">
    <p style={{color:'black'}} name={props.name} id={props.id}>{props.name}</p>
  </Link>
);

export default AlbumThumb;