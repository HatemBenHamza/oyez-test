//Import React stuff
import React, { Component } from 'react';
import { Link } from 'react-router';
// Import component
import $ from 'jquery';
import { Button} from 'react-bootstrap';




// Create main React component
class Album extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos:[],
      referrer: null,
    };
    this.thisAlbum = props.albums.filter(
      album => album.title === props.params.albumName
    );
  }

  getAlbums(){
  $.ajax({
    url: 'https://jsonplaceholder.typicode.com/photos',
    dataType:'json',
    cache: false,
    success: function(data){
       const photos = data.filter(
          photo => photo.albumId === this.thisAlbum[0].id
      );
      this.setState({photos: photos}, function(){
        console.log(this.state);
      });
    //console.log(photos);
    }.bind(this),
    error: function(xhr, status, err){
      console.log(err);
    }
  });
  }


  componentDidMount(){
    this.getAlbums();
  }

  render(){
    const listItems = this.state.photos.map((d) => {
    return (
    <div style={{margin: 'auto',width: '50%'}}>
     <Link to={d.url} className="thumb">
     <img  src={d.thumbnailUrl} albumName={this.thisAlbum[0].name} url={d.url} key={d.title} style={{paddinTop:'15px'}}/>
    </Link>
    
    <Button color="primary">
      <Link to='/wishelist' image={d.url} 
      style={{color:'black'}}>Add to wishelist</Link>
    </Button>
    </div>
    ); 
    });
    const {referrer} = this.state;
    if (referrer) return <Redirect to={referrer} />;
    return (
      <div>
        {listItems}
        
      </div>
    );
  }
}

export default Album;