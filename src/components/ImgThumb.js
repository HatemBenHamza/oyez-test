//Import React stuff
import React, { Component } from 'react';

// Create main React component
class ImgTumb extends Component {
  constructor(props) {
    super(props);
    this.state = {
    
    };
  }
  render(){
    return (
      <div>
      
      <h4>{this.props.albumNAme}</h4>
      <img src={this.props.url}/>        
      </div>
    );
  }
}

export default ImgTumb;