// Import React stuff
import React, { Component } from 'react';
// Create main React component
class Wishelist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images:[...props.url]
    };
  }


  render() {
    const wisheListItems = this.state.images.map((d) => {
      return (
      <div>
        <img src={d.url}/>> 
      </div>
        ); 
      });

      <div>
        <h1 style={{textAlign:'center'}}>Wishelist page</h1>
        {wisheListItems}
      </div>
  }
}

export default Wishelist;