// Import React stuff
import React, { Component } from 'react';
import { Link } from 'react-router';
// Import compiled CSS
import './App.css';
// Import photo album info
import data from './data';
import $ from 'jquery';


// Create main React component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albumsFromAPI:[],
    };
  }

  getTodos(){
    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/albums',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({albumsFromAPI: data}, function(){
          //console.log(this.state);
        });
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
    });
  }



  componentDidMount(){
    this.getTodos();
  }

  render() {
    // Clone child components giving them props from the state
    const childrenWithProps = React.Children.map(this.props.children, child =>
      React.cloneElement(child, {
        albums: this.state.albumsFromAPI,
      })
    );
    return (
      <div>
        <div className="page-wrap">
          <div className="header">
            <h1>
              <Link to="/">React Photo Album</Link>
            </h1>
          </div>
          {childrenWithProps}
        </div>
      </div>
    );
  }
}

export default App;