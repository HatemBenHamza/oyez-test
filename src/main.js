// Import React stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

// Import components
import App from './App';
import Albums from './components/Albums';
import Album from './components/Album';
import Wishelist from './components/Wishelist';
import ImgThumb from './components/ImgThumb';

const router = (
  <Router history={browserHistory}>
    <Route exact path="/" component={App}>
      <IndexRoute component={Albums} />
      <Route path=":albumName" component={Album} />
      <Route path=":albumName/:photoId" component={ImgThumb} />
    </Route>
    <Route path="/wishelist" component={Wishelist} />
  </Router>
);

ReactDOM.render(router, document.getElementById('root'));